<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['usuario'] = 'usuario';
$route['admin'] = 'admin';
$route['verifica'] = 'admin/verifica';
$route['logout'] = 'admin/logout';
$route['admin/menu'] = 'admin/menu';

$route['contacto'] = 'contacto';
$route['contacto/envio_correo'] = 'contacto/envio_correo';

$route['pais-back'] = 'pais_back';
$route['pais-back/registrar'] = 'pais_back/registrar';
$route['pais-back/editar/(:any)'] = 'pais_back/editar/$1';
$route['pais-back/editado/(:any)'] = 'pais_back/editado/$1';
$route['pais-back/eliminar/(:any)'] = 'pais_back/eliminar/$1';

$route['estado-back'] = 'estado_back';
$route['estado-back/registrar'] = 'estado_back/registrar';
$route['estado-back/editar/(:any)'] = 'estado_back/editar/$1';
$route['estado-back/editado/(:any)'] = 'estado_back/editado/$1';
$route['estado-back/eliminar/(:any)'] = 'estado_back/eliminar/$1';
