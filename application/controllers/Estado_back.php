<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Estado_back extends CI_Controller
{

	public function __construct()
    {
        parent::__construct();
        $this->load->model('Estado_model');
		$this->load->model('Pais_model');
    }

    public function index()
    {
        if ($this->session->userdata('login'))
        {
            $data['listado'] = $this->Estado_model->obtener_estados();
			$data['paises'] = $this->Pais_model->obtener_paises();
            $data['campos'] = $this->_form_fields();
            $this->load->view('back/header');
            $this->load->view('back/nav');
            $this->load->view('back/estado/estado-content', $data);
            $this->load->view('back/footer');
        }
        else
        {
            redirect('admin');
        }
    }



    public function registrar()
    {
        $this->form_validation->set_rules('nombre_estado','El nombre del estado/provincia','required|is_unique[estados.nombre]|max_length[100]|xss_clean');
        $this->form_validation->set_message('required','%s es requerido');
		$this->form_validation->set_message('is_unique','%s ya está registrado');
        if ($this->form_validation->run() == TRUE)
        {
            $nombre = $this->input->post('nombre_estado');
			$id_pais = $this->input->post('pais_id');
			$usuario = $this->session->userdata('usuario');
            $subir = $this->Pais_model->registrar_estados($nombre_estado,$pais_id,$usuario);
			$this->_mensaje("registrar",$subir);
        }
        else
        {
          //  echo validation_errors();
          //  exit;
            // $this->index();
			$this->index();
        }

        // redirect('destinos');
    }

    public function eliminar($id_estado)
    {
        if (!$this->session->userdata('login'))
        {
            redirect("estado/index");
        }
        else
        {
            $this->Pais_model->eliminar_estados($id_estado);
            $this->_mensaje("eliminar", TRUE);
        }
    }

    public function editar($id_estado)
    {
        if (!$this->session->userdata('login'))
        {
            redirect("pais/index");
        }
        else
        {
			$data['listado'] = $this->Pais_model->editar_estados($id_estado);
            $data['campos'] = $this->_form_fields();
			// $data['id'] = $id;
            $this->load->view('back/header');
            $this->load->view('back/nav');
            $this->load->view('back/estado/estado-content-edit', $data);
			$this->load->view('back/footer');
        }
    }

	public function editado($id_estado)
	{
		$this->form_validation->set_rules('nombre','Estado','is_unique[estados.nombre]|required|max_length[100]|xss_clean');
        $this->form_validation->set_message('required','%s es requerido');
		$this->form_validation->set_message('is_unique','%s ya existe');
        if ($this->form_validation->run() == TRUE)
        {
            $nombre = $this->input->post('nombre_estado');
			$usuario = $this->session->userdata('usuario');
			$id_pais = $this->input->post('pais_id');
            $subir = $this->Pais_model->actualizar_estados($id_estado,$nombre_estado,$pais_id,$usuario);
			$this->_mensaje("registrar",$subir);
        }
        else
        {
           	//  echo validation_errors();
           	//  exit;
           	// $this->index();
			$this->index();
        }
	}


    private function _mensaje($accion,$sino)
    {
        $this->load->view('back/header');
        $this->load->view('back/nav');
        if ($sino)
        {
            switch ($accion)
            {
                case 'registrar':
                    $datos = array('ruta' =>'pais-back', 'mensaje' => 'El estado/provincia fué registrado!');
                    break;
                case 'eliminar':
                    $datos = array('ruta' =>'pais-back', 'mensaje' => 'El estado/provincia fué eliminado!');
                    break;
                case 'editar':
                    $datos = array('ruta' =>'pais-back', 'mensaje' => 'El estado/provincia fué modificado!');
                    break;
            }
        }
        else
        {
            $datos = array('ruta' =>'estado-back', 'mensaje' => 'Ha ocurrido un error, comuniquese con el administrador');
        }
        $this->load->view('back/mensaje', $datos);
		$this->load->view('back/footer');
    }

	function _mensaje_error($merror)
	{
		$this->load->view('back/header');
        $this->load->view('back/nav');
		$this->load->view('back/mensaje-error', $merror);
		$this->load->view('back/footer');
	}

    private function _form_fields()
    {
        $arreglo = array(
            'nombre_estado'        => array(
                'name'          =>    'nombre_estado',
                'id'            =>    'nombre_estado',
                'value'         =>    set_value('nombre_estado'),
                'placeholder'   =>    'Nombre del Estado/Provincia',
                'size'          =>    '50',
                'maxlenght'     =>    '100',
                'rows'          =>    '2',
                'class'         =>    'form-control'
            ),

			'pais_id'        => array(
                'name'          =>    'pais_id',
                'id'            =>    'pais_id',
                'value'         =>    set_value('pais_id'),
                'class'         =>    'form-control'
            ),

            'guardar'       => array(
                'name'          =>    'guardar',
                'id'            =>    'guardar',
                'value'         =>    'Guardar',
                'class'         =>    'btn btn-primary'
            )
        );

        return $arreglo;
    }

}
