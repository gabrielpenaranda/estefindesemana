<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pais_back extends CI_Controller
{

	public function __construct()
    {
        parent::__construct();
        $this->load->model('Pais_model');
    }

    public function index()
    {
        if ($this->session->userdata('login'))
        {
            $data['listado'] = $this->Pais_model->obtener_paises();
            $data['campos'] = $this->_form_fields();
            $this->load->view('back/header');
            $this->load->view('back/nav');
            $this->load->view('back/pais/pais-content', $data);
            $this->load->view('back/footer');
        }
        else
        {
            redirect('admin');
        }
    }



    public function registrar()
    {
        $this->form_validation->set_rules('nombre_pais','El nombre del país','required|is_unique[paises.nombre]|max_length[100]|xss_clean');
        $this->form_validation->set_message('required','%s es requerido');
		$this->form_validation->set_message('is_unique','%s ya está registrado');
        if ($this->form_validation->run() == TRUE)
        {
            $nombre = $this->input->post('nombre_pais');
			$usuario = $this->session->userdata('usuario');
            $subir = $this->Pais_model->registrar_paises($nombre_pais,$usuario);
			$this->_mensaje("registrar",$subir);
        }
        else
        {
          //  echo validation_errors();
          //  exit;
            // $this->index();
			$this->index();
        }

        // redirect('destinos');
    }

    public function eliminar($id_pais)
    {
        if (!$this->session->userdata('login'))
        {
            redirect("pais/index");
        }
        else
        {
            $this->Pais_model->eliminar_paises($id_pais);
            $this->_mensaje("eliminar", TRUE);
        }
    }

    public function editar($id_pais)
    {
        if (!$this->session->userdata('login'))
        {
            redirect("pais/index");
        }
        else
        {
			$data['listado'] = $this->Pais_model->editar_paises($id_pais);
            $data['campos'] = $this->_form_fields();
			// $data['id'] = $id;
            $this->load->view('back/header');
            $this->load->view('back/nav');
            $this->load->view('back/pais/pais-content-edit', $data);
			$this->load->view('back/footer');
        }
    }

	public function editado($id_pais)
	{
		$this->form_validation->set_rules('nombre_pais','El nombre del país','is_unique[paises.nombre]|required|max_length[100]|xss_clean');
        $this->form_validation->set_message('required','%s es requerido');
		$this->form_validation->set_message('is_unique','%s ya está registrado');
        if ($this->form_validation->run() == TRUE)
        {
            $nombre = $this->input->post('nombre_pais');
			$usuario = $this->session->userdata('usuario');
            $subir = $this->Pais_model->actualizar_paises($id_pais,$nombre_pais,$usuario);
			$this->_mensaje("modificar",$subir);
        }
        else
        {
           	//  echo validation_errors();
           	//  exit;
           	// $this->index();
			$this->index();
        }
	}


    private function _mensaje($accion,$sino)
    {
        $this->load->view('back/header');
        $this->load->view('back/nav');
        if ($sino)
        {
            switch ($accion)
            {
                case 'registrar':
                    $datos = array('ruta' =>'pais-back', 'mensaje' => 'El país fué registrado!');
                    break;
                case 'eliminar':
                    $datos = array('ruta' =>'pais-back', 'mensaje' => 'El país fué eliminado!');
                    break;
                case 'editar':
                    $datos = array('ruta' =>'pais-back', 'mensaje' => 'El país fué modificado!');
                    break;
            }
        }
        else
        {
            $datos = array('ruta' =>'pais-back', 'mensaje' => 'Ha ocurrido un error, comuniquese con el administrador');
        }
        $this->load->view('back/mensaje', $datos);
		$this->load->view('back/footer');
    }

	function _mensaje_error($merror)
	{
		$this->load->view('back/header');
        $this->load->view('back/nav');
		$this->load->view('back/mensaje-error', $merror);
		$this->load->view('back/footer');
	}

    private function _form_fields()
    {
        $arreglo = array(
            'nombre_pais'        => array(
                'name'          =>    'nombre_pais',
                'id'            =>    'nombre_pais',
                'value'         =>    set_value('nombre_pais'),
                'placeholder'   =>    'Nombre del país',
                'size'          =>    '50',
                'maxlenght'     =>    '100',
                'rows'          =>    '2',
                'class'         =>    'form-control'
            ),

            'guardar'       => array(
                'name'          =>    'guardar',
                'id'            =>    'guardar',
                'value'         =>    'Guardar',
                'class'         =>    'btn btn-primary'
            )
        );

        return $arreglo;
    }

}
