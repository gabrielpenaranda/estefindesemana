<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Estado_model extends CI_Model
{

	public function __construct()
    {
		parent::__construct();
    }

    public function obtener_estados()
    {
        $r = $this->db->get('estados');
        if ($r->num_rows() > 0)
        {
            return $r;
        }
        else
        {
            return NULL;
        }
    }

    public function registrar_estados($nombre_estado,$usuario)
    {
        $estado = array(
                'nombre_estado'	=>  $nombre_estado,
				'pais_id'		=>  $pais_id,
				'posted_user'	=>	$usuario
             );
        return $this->db->insert('estados', $estado);
    }

	public function actualizar_estados($id_estado,$nombre_estado,$usuario)
    {
        $estado = array(
				'nombre_estado'	=>  $nombre_estado,
				'pais_id'		=>  $pais_id,
				'posted_user'	=>	$usuario
             );
		$this->db->where('id', $id_estado);
        return $this->db->update('estados', $estado);
    }

    public function eliminar_estados($id_estado)
	{
        $this->db->where('id',$id_estado);
        $r=$this->db->delete('estados');
    }

	public function editar_estados($id_estado)
	{
		$this->db->where('id',$id_estado);
        $r=$this->db->get('estados');
		return $r->row();
    }


}
