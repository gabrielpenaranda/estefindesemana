<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pais_model extends CI_Model
{

	public function __construct()
    {
		parent::__construct();
    }

    public function obtener_paises()
    {
		$this->db->select('id_pais,nombre_pais');
		$this->db->order_by('nombre_pais', 'ASC');
        $r = $this->db->get('paises');
		// echo var_dump($r->result());
		// exit;
        if ($r->num_rows() > 0)
        {
            return $r;
        }
        else
        {
            return NULL;
        }
    }

    public function registrar_paises($nombre_pais,$usuario)
    {
        $pais = array(
                'nombre_pais'   =>  $nombre_pais,
				'posted_user'	=>	$usuario
             );
        return $this->db->insert('paises', $pais);
    }

	public function actualizar_paises($id_pais,$nombre_pais,$usuario)
    {
        $pais = array(
                'nombre_pais'   =>  $nombre_pais,
				'posted_user'	=>	$pusuario
             );
		$this->db->where('id', $id_pais);
        return $this->db->update('paises', $pais);
    }

    public function eliminar_paises($id_pais)
	{
        $this->db->where('id',$id_pais);
        $r=$this->db->delete('paises');
    }

	public function editar_paises($id_pais)
	{
		$this->db->where('id',$id_pais);
        $r=$this->db->get('paises');
		return $r->row();
    }


}
