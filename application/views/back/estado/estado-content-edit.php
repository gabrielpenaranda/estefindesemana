<section id="contenido-edita-pais">

    <div class="container">
        <div class="row">

            <div class="col-xs-offset-3 col-xs-6">
                <h3 class="text-center">Registro de Países <small>Modificar Datos</small></h3>
            </div>

            <div class="col-xs-offset-1 col-xs-10">

                <?php
                $campos['nombre']['value'] = $listado->nombre_estado;

                echo validation_errors("<div class='alert alert-danger'>","</div>");

                echo form_open_multipart(base_url('pais-back/editado').'/'.$listado->id_estado);

                echo '<div class="form-group">';
                echo form_label('País:', "nombre_estado");
                echo form_input($campos['nombre_estado']);
                echo '</div>';

                echo '<div class="form-group">';
                echo form_submit($campos['guardar']);
                echo '</div>';

                echo form_close();
                ?>

            </div>
        </div>
    </div>

</section>
