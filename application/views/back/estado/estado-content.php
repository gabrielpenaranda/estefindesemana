<section id="contenido-registro-pais">

    <div class="container">
        <div class="row">

            <div class="col-xs-offset-3 col-xs-6">
                <h3 class="text-center">Registro de Países</h3>
            </div>

            <div class="col-xs-offset-1 col-xs-10">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation"><a href="#consulta" aria-controls="profile" role="tab" data-toggle="tab">CONSULTA</a></li>
                    <li role="presentation"><a href="#registro" aria-controls="home" role="tab" data-toggle="tab">REGISTRO</a></li>
                </ul>

                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane" id="consulta">
                        <table class="table table-striped">
                            <thead>
                                <th class="text-center">País</th>
                                <th class="text-center">Acciones</th>
                            </thead>
                            <tbody>
                                <?php
                                if ($listado != NULL):
                                    foreach($listado->result() as $p):?>
                                    <tr>
                                        <td><?php echo $p->nombre_pais; ?></td>
                                        <td class="text-center">
                                            <a href="<?php echo base_url('pais-back/eliminar/').$p->id; ?>" title="Eliminar"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                                            <a href="<?php echo base_url('pais-back/editar/').$p->id; ?>" title="Editar"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                                        </td>
                                    </tr>
                                    <?php
                                endforeach;
                            endif;?>
                        </tbody>
                    </table>
                </div>

                <div role="tabpanel" class="tab-pane active" id="registro">

                    <?php
                    echo validation_errors("<div class='alert alert-danger'>","</div>");

                    echo form_open_multipart(base_url('pais-back/registrar'));

                    echo '<div class="form-group">';
                    echo form_label('País:', "nombre_pais");
                    echo form_input($campos['nombre_pais']);
                    echo '</div>';

                    echo '<div class="form-group">';
                    echo form_submit($campos['guardar']);
                    echo '</div>';

                    echo form_close();
                    ?>

                    </div>

                </div>
            </div>
        </div>
    </div>

</section>
