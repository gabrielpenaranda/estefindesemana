<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Este fin de semana, donde encontrara que hacer y donde ir">
    <meta name="keywords" content="viajes, turismo, destinos, entretenimiento, teatro, fiestas, discotecas, eventos, restaurantes, museos">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ESTE FIN DE SEMANA-donde encontrara que hacer y donde ir</title>
    <link href='http://fonts.googleapis.com/css?family=Amaranth' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo base_url() ?>resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>resources/jquery-ui/jquery-ui.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>resources/jquery-ui/jquery-ui.structure.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>resources/jquery-ui/jquery-ui.theme.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>resources/css/app.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>resources/css/app-back.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>resources/css/btstrp-back.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
