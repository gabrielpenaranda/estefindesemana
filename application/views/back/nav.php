     <div class="container-fluid">

        <div class="row pw-header-back">

            <div class="col-xs-12">
                <h2 class="pw-brand">Este fin de semana <small> Módulo de Administración</small></h2>
            </div>

            <div class="col-xs-12">
                <nav class="navbar navbar-default">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapsed-bar-1" aria-expanded="false">
                            <span class="sr-only">Menu</span>
                            <span class="icon-bar pw-color-1"></span>
                            <span class="icon-bar pw-color-1"></span>
                            <span class="icon-bar pw-color-1"></span>
                        </button>
                       <!-- <a class="navbar-brand pw-brand" href="#">Este fin de semana</a> -->
                    </div>

                    <?php if ($this->session->userdata('login')): ?>

                    <div class="collapse navbar-collapse" id="collapsed-bar-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Tablas <span class="caret"></span></a>
                                <ul class="dropdown-menu pw-menu">
                                    <li><a href="#">Ciudades</a></li>
                                    <li><a href="<?php echo base_url('estado-back')?>">Estados</a></li>
                                    <li><a href="<?php echo base_url('pais-back')?>">Países</a></li>
                                    <li><a href="#">Tipo de eventos</a></li>
                                    <li><a href="#">Tipo de lugares</a></li>
                                    <li><a href="#">Imágenes de lugares</a></li>
                                </ul>
                            </li>
                            <li><a href="c">Lugares</a></li>
                            <li><a href="<?php //echo base_url('paquetes-back')?>">Eventos</a></li>
                            <li><a href="<?php echo base_url('logout') ?>">Salir</a></li>
                        </ul>
                    </div>

                  <?php else: ?>
                          <div class="collapse navbar-collapse" id="collapsed-bar-1">
                              <ul class="nav navbar-nav navbar-right">
                                  <li><a href="<?php echo base_url() ?>admin">Iniciar sesión</a></li>
                              </ul>
                         </div>
                  <?php endif; ?>

                </nav>
            </div>
        </div>

      </div>
