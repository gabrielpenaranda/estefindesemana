        <footer>
            <div class="container-fluid">
               <div class="row">
                    <div class="col-xs-12 pw-footer">
                        <br><br><br><br>
                        <h5 class="pw-color-1">&copy2017 www.paginas-web.com.ve</h5>
                    </div>
               </div>
            </div>
        </footer>

        <script src="<?php echo base_url() ?>resources/js/jquery.min.js"></script>
        <script src="<?php echo base_url() ?>resources/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>resources/js/main.js"></script>
        <script src="<?php echo base_url() ?>resources/js/carousel-fade.js"></script>
<!--        <script src="<?php //echo base_url() ?>resources/js/material.min.js"></script>-->
<!--        <script src="<?php //echo base_url() ?>resources/js/ripples.min.js"></script>-->
<!--
        <script>
            $(document).ready(function(){
                
            });
        </script>
-->

    </body>
</html>
