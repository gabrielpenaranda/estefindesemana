<header>
    <hgroup>
        <h1 class="pw-oculto">Este fin de semana</h1>
        <h2 class="pw-oculto">donde encontrara que hacer y donde ir</h2>
    </hgroup>
</header>

<nav class="navbar navbar-default pw-menu">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar pw-color-1"></span>
                <span class="icon-bar pw-color-1"></span>
                <span class="icon-bar pw-color-1"></span>
            </button>
            <a class="navbar-brand pw-brand" href="#">Este fin de semana</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Este fin de semana en... <span class="caret"></span></a>
                    <ul class="dropdown-menu pw-menu">
                        <li><a href="#">Barquisimeto</a></li>
                        <li><a href="#">Cabudare</a></li>
                    </ul>
                </li>
                <li><a href="#">Contacto</a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>