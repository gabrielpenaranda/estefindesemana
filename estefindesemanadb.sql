-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 16-02-2017 a las 04:22:47
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `estefindesemanadb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudades`
--

CREATE TABLE `ciudades` (
  `id_ciudad` int(11) NOT NULL,
  `nombre_ciudad` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `estado_id` int(11) NOT NULL,
  `posted_user` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `posted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELACIONES PARA LA TABLA `ciudades`:
--   `estado_id`
--       `estados` -> `id_estado`
--

--
-- Volcado de datos para la tabla `ciudades`
--

INSERT INTO `ciudades` (`id_ciudad`, `nombre_ciudad`, `estado_id`, `posted_user`, `posted`) VALUES
(1, 'Barquisimeto', 1, 'gpg', '2017-02-16 02:10:01'),
(2, 'San Felipe', 2, 'gpg', '2017-02-16 02:10:01'),
(3, 'Cabudare', 1, 'gpg', '2017-02-16 02:10:26'),
(4, 'Yaritagua', 2, 'gpg', '2017-02-16 02:10:41'),
(5, 'Cucuta', 3, 'gpg', '2017-02-16 02:36:46'),
(6, 'Cali', 4, 'gpg', '2017-02-16 02:37:03'),
(7, 'Maracaibo', 5, 'gpg', '2017-02-16 02:37:50'),
(8, 'Maracay', 6, 'gpg', '2017-02-16 02:38:12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados`
--

CREATE TABLE `estados` (
  `id_estado` int(11) NOT NULL,
  `nombre_estado` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `pais_id` int(11) NOT NULL,
  `posted_user` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `posted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELACIONES PARA LA TABLA `estados`:
--   `pais_id`
--       `paises` -> `id_pais`
--

--
-- Volcado de datos para la tabla `estados`
--

INSERT INTO `estados` (`id_estado`, `nombre_estado`, `pais_id`, `posted_user`, `posted`) VALUES
(1, 'Lara', 1, 'admin', '2017-02-16 02:09:08'),
(2, 'Yaracuy', 1, 'gpg', '2017-02-16 02:09:08'),
(3, 'Norte de Santander', 4, 'gpg', '2017-02-16 02:34:03'),
(4, 'Valle del Cauca', 4, 'gpg', '2017-02-16 02:35:02'),
(5, 'Zulia', 1, 'gpg', '2017-02-16 02:34:35'),
(6, 'Aragua', 1, 'gpg', '2017-02-16 02:35:11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `eventos`
--

CREATE TABLE `eventos` (
  `id_lugar` int(11) NOT NULL,
  `nombre_evento` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `lugar_id` int(11) NOT NULL,
  `descripcion_evento` text COLLATE utf8_unicode_ci NOT NULL,
  `tipo_lugar_id` int(11) NOT NULL,
  `fecha_evento` date NOT NULL,
  `hora_evento` time NOT NULL,
  `posted_user` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `posted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELACIONES PARA LA TABLA `eventos`:
--   `lugar_id`
--       `lugares` -> `id_lugar`
--   `tipo_lugar_id`
--       `tipo_eventos` -> `id_tipo_evento`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagen_lugares`
--

CREATE TABLE `imagen_lugares` (
  `id_imagen_lugar` int(11) NOT NULL,
  `lugar_id` int(11) NOT NULL,
  `imagen_lugar` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `thumb_imagen_lugar` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `posted_user` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `posted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELACIONES PARA LA TABLA `imagen_lugares`:
--   `lugar_id`
--       `lugares` -> `id_lugar`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lugares`
--

CREATE TABLE `lugares` (
  `id_lugar` int(11) NOT NULL,
  `nombre_lugar` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion_lugar` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `direccion_lugar` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `ciudad_id` int(11) NOT NULL,
  `tipo_lugar_id` int(11) NOT NULL,
  `posted_user` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `posted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELACIONES PARA LA TABLA `lugares`:
--   `ciudad_id`
--       `ciudades` -> `id_ciudad`
--   `tipo_lugar_id`
--       `tipo_lugares` -> `id_tipo_lugar`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paises`
--

CREATE TABLE `paises` (
  `id_pais` int(11) NOT NULL,
  `nombre_pais` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `posted_user` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `posted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELACIONES PARA LA TABLA `paises`:
--

--
-- Volcado de datos para la tabla `paises`
--

INSERT INTO `paises` (`id_pais`, `nombre_pais`, `posted_user`, `posted`) VALUES
(1, 'Venezuela', 'gpg', '2017-02-16 02:07:56'),
(3, 'Paraguay', 'gpg', '2017-02-12 23:16:09'),
(4, 'Colombia', 'admin', '2017-02-16 02:08:10'),
(5, 'Bolivia', 'admin', '2017-02-12 22:43:07'),
(6, 'Brasil', 'admin', '2017-02-12 22:43:40'),
(7, 'Chile', 'gpg', '2017-02-12 22:48:36'),
(8, 'Argentina', 'gpg', '2017-02-13 01:07:22');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_eventos`
--

CREATE TABLE `tipo_eventos` (
  `id_tipo_evento` int(11) NOT NULL,
  `tipo_evento` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `posted_user` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `posted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELACIONES PARA LA TABLA `tipo_eventos`:
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_lugares`
--

CREATE TABLE `tipo_lugares` (
  `id_tipo_lugar` int(11) NOT NULL,
  `tipo_lugar` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `posted_user` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `posted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELACIONES PARA LA TABLA `tipo_lugares`:
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `usuario` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(300) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELACIONES PARA LA TABLA `usuarios`:
--

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `usuario`, `password`) VALUES
(1, 'admin', '150f6488fdf7f08e1573b1c4fc66b326'),
(2, 'gpg', '013365df0fa8bd11a96aac558f1ea973');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `ciudades`
--
ALTER TABLE `ciudades`
  ADD PRIMARY KEY (`id_ciudad`),
  ADD KEY `id_estado` (`estado_id`);

--
-- Indices de la tabla `estados`
--
ALTER TABLE `estados`
  ADD PRIMARY KEY (`id_estado`),
  ADD KEY `id_pais` (`pais_id`);

--
-- Indices de la tabla `eventos`
--
ALTER TABLE `eventos`
  ADD PRIMARY KEY (`id_lugar`),
  ADD KEY `id_lugar` (`lugar_id`),
  ADD KEY `id_tipo` (`tipo_lugar_id`);

--
-- Indices de la tabla `imagen_lugares`
--
ALTER TABLE `imagen_lugares`
  ADD PRIMARY KEY (`id_imagen_lugar`),
  ADD KEY `id_lugar` (`lugar_id`);

--
-- Indices de la tabla `lugares`
--
ALTER TABLE `lugares`
  ADD PRIMARY KEY (`id_lugar`),
  ADD KEY `id_ciudad` (`ciudad_id`,`tipo_lugar_id`),
  ADD KEY `id_tipo` (`tipo_lugar_id`);

--
-- Indices de la tabla `paises`
--
ALTER TABLE `paises`
  ADD PRIMARY KEY (`id_pais`);

--
-- Indices de la tabla `tipo_eventos`
--
ALTER TABLE `tipo_eventos`
  ADD PRIMARY KEY (`id_tipo_evento`);

--
-- Indices de la tabla `tipo_lugares`
--
ALTER TABLE `tipo_lugares`
  ADD PRIMARY KEY (`id_tipo_lugar`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `ciudades`
--
ALTER TABLE `ciudades`
  MODIFY `id_ciudad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `estados`
--
ALTER TABLE `estados`
  MODIFY `id_estado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `eventos`
--
ALTER TABLE `eventos`
  MODIFY `id_lugar` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `imagen_lugares`
--
ALTER TABLE `imagen_lugares`
  MODIFY `id_imagen_lugar` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `lugares`
--
ALTER TABLE `lugares`
  MODIFY `id_lugar` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `paises`
--
ALTER TABLE `paises`
  MODIFY `id_pais` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `tipo_eventos`
--
ALTER TABLE `tipo_eventos`
  MODIFY `id_tipo_evento` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tipo_lugares`
--
ALTER TABLE `tipo_lugares`
  MODIFY `id_tipo_lugar` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `ciudades`
--
ALTER TABLE `ciudades`
  ADD CONSTRAINT `ciudades_ibfk_1` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id_estado`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `estados`
--
ALTER TABLE `estados`
  ADD CONSTRAINT `estados_ibfk_1` FOREIGN KEY (`pais_id`) REFERENCES `paises` (`id_pais`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `eventos`
--
ALTER TABLE `eventos`
  ADD CONSTRAINT `eventos_ibfk_1` FOREIGN KEY (`lugar_id`) REFERENCES `lugares` (`id_lugar`) ON UPDATE CASCADE,
  ADD CONSTRAINT `eventos_ibfk_2` FOREIGN KEY (`tipo_lugar_id`) REFERENCES `tipo_eventos` (`id_tipo_evento`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `imagen_lugares`
--
ALTER TABLE `imagen_lugares`
  ADD CONSTRAINT `imagen_lugares_ibfk_1` FOREIGN KEY (`lugar_id`) REFERENCES `lugares` (`id_lugar`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `lugares`
--
ALTER TABLE `lugares`
  ADD CONSTRAINT `lugares_ibfk_1` FOREIGN KEY (`ciudad_id`) REFERENCES `ciudades` (`id_ciudad`) ON UPDATE CASCADE,
  ADD CONSTRAINT `lugares_ibfk_2` FOREIGN KEY (`tipo_lugar_id`) REFERENCES `tipo_lugares` (`id_tipo_lugar`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
